package main.java;

/**
 * Class gathering the combination of perturbations and thus the combination of perturbed nodes. 
 */

import java.util.ArrayList;
import java.util.List;


public class CombinationPerturbations {
	List<DrugObject> DrugObject; 
	String mapNodes; // perturbed nodes, separated with comma, compliant with ginsim
	
	public CombinationPerturbations(){
		this.DrugObject = new ArrayList<DrugObject>();
	}

	public List<DrugObject> getDrugObject() {
		return DrugObject;
	}

	public void setDrugObject(List<DrugObject> drugObject) {
		this.DrugObject = drugObject;
	}
	
	public void addDrugObject(DrugObject dg){
		this.DrugObject.add(dg);
	}

	public String getMapNodes() {
		return mapNodes;
	}

	public void setMapNodes(String mapNodes) {
		this.mapNodes = mapNodes;
	}
}