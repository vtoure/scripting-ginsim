package main.java;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.colomoto.biolqm.LQMScriptLauncher;
import org.colomoto.biolqm.LQMServiceManager;
import org.colomoto.biolqm.LogicalModel;
import org.colomoto.biolqm.tool.stablestate.StableStateList;
import org.colomoto.biolqm.tool.stablestate.StableStateTool;
import org.ginsim.ScriptLauncher;
import org.ginsim.common.application.GsException;
import org.ginsim.core.graph.regulatorygraph.RegulatoryGraph;


public class Main {
	
	public String filenameModel = "";
	public String filenamePerturbations = "";
	public String filenameMapping = "";
	public String filenameOutput = "";


	public List<DrugObject> perturbation = new ArrayList<DrugObject>();
	public List<CombinationPerturbations> combi = new ArrayList<CombinationPerturbations>();
	
	public static void main(String[] args) throws Exception{
		Main simulate = new Main();
		simulate.setInputs(args);
		
		try {
			ScriptLauncher launcher = new ScriptLauncher();
			LogicalModel model = ((RegulatoryGraph)launcher.load(simulate.filenameModel)).getModel();
			simulate.getMapping();
			simulate.getListNodesPerturbed();

			// Launch LQM for computing perturbations and stable states. 
			LQMScriptLauncher lqm = new LQMScriptLauncher(args);
			PrintWriter writer = new PrintWriter(new File(simulate.filenameOutput) , "UTF-8");
			
			//Head of output file: list of order of the nodes 
			writer.println(model.getComponents());
			
			System.out.println("Computing stable states for each perturbation...");
				for(CombinationPerturbations cop : simulate.combi){
					LogicalModel perturbed = lqm.modify(model, "perturbation", cop.getMapNodes()); //cop.getMapNodes = string of knockouts in ginsim format: A%0,B%0
					StableStateTool stableState = LQMServiceManager.getTool(StableStateTool.class);
					StableStateList stableStateResults = stableState.getResult(perturbed);					
					
					if(stableStateResults.size() == 0){
						writer.println(cop.getMapNodes() + "\t no stable state found");
					}
					else{
						for(int[] list : stableStateResults){
							String stablestate = "";
							for(int i = 0 ; i < list.length; i++)
								stablestate += list[i]+ "-";
							stablestate = stablestate.substring(0, stablestate.length() - 1);
							writer.println(cop.getMapNodes() + "\t" + stablestate);
						}
					}
					writer.flush();
				}			
			writer.close();
			System.out.println("Computation done. Results are available in " + simulate.filenameOutput);
		} catch (GsException e) {
			e.printStackTrace();
			e.getCause();
			
		} catch (IOException e) {
			e.printStackTrace();
			e.getCause();
		}
		
	}
	
	public void setInputs(String[] args){
		if(args.length == 5){
			filenameModel = args[0];
			filenamePerturbations = args[1];
			filenameMapping = args[2];
			filenameOutput = args[4];
			
		}
		else{
			System.err.println("Arguments missing. \n Proper usage: java  <model file - ginml> <perturbations file> <mapping perturbations to nodes file> -output <output file name>");
		}
	}
	
	
	/**
	 * Mapping drugs and perturbed nodes: retrieve a DrugObject
	 * ID: drug
	 * target: list of perturbed nodes - GINSIM KO style (name + %0/%1)
	 * @throws IOException
	 */
	private void getMapping() throws IOException{
		BufferedReader br = readFile(filenameMapping);
		String line;	
		
		while((line = br.readLine()) != null){
			if(!(line.startsWith("#"))){
				DrugObject dg = new DrugObject();
				dg.setId(line.split("\t")[0]);
				for(int i = 2; i <line.split("\t").length; i++){
					if(line.split("\\t")[1].equals("inhibits"))
						dg.addTargetNode(line.split("\t")[i]+"%0");
					else //should be 'activates' - other cases?
						dg.addTargetNode(line.split("\t")[i]+"%1");
				}
				perturbation.add(dg);
			}	
		}
	}
	
	/**
	 * Get the list of knockout nodes:
	 * Comparison of list with the combination of drugs and 
	 * list of drugObject (map drugs to perturbed nodes).
	 * 
	 * @throws IOException
	 */
	public void getListNodesPerturbed() throws IOException{
		BufferedReader br = readFile(filenamePerturbations);
		String line;
		
		while((line = br.readLine()) != null){
			CombinationPerturbations cop = new CombinationPerturbations();
			String concatNode = "";
			for(int i = 0; i < line.split("\t").length; i++){

				for(DrugObject dg : perturbation){
					if(dg.getId().equals(line.split("\t")[i])){
						cop.addDrugObject(dg);
						for(String s : dg.getTargetNode())
							concatNode=concatNode + s +",";
					}	
				}	
			}
			
			concatNode = concatNode.substring(0, concatNode.length() - 1);
			cop.setMapNodes(concatNode);
			combi.add(cop);
		}
	}
	
	/**
	 * Simple file reader from file name
	 * @param filename
	 * @return BufferedReader
	 * @throws FileNotFoundException
	 */
	private BufferedReader readFile(String filename) throws FileNotFoundException{
		File f = new File(filename);
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		return br;
	}
}