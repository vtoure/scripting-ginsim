package main.java;

/**
 * @author vasundra toure
 * 
 * Perturbation/Drug object with the list of perturbed nodes.
 * On drug can perturb many nodes.
 */

import java.util.ArrayList;
import java.util.List;

public class DrugObject {


	public String id; // Perturbation ID
	public List<String> targetNode; // List of nodes perturbed, in ginsim format: <node>%<state> ex: AKT%0

	public DrugObject() {
		this.targetNode = new ArrayList<String>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getTargetNode() {
		return targetNode;
	}

	public void setTargetNode(List<String> targetNode) {
		this.targetNode = targetNode;
	}
	
	public void addTargetNode(String targetNode){
		this.targetNode.add(targetNode);
	}
}