# Compute stable states #

This script computes stable states for multiple combination of perturbations using bioLQM and GINsim libraries.

## Compile ##
```
javac -d build -cp ./lib/bioLQM-0.5-SNAPSHOT-jar-with-dependencies.jar:./lib/GINsim-2.9.6-SNAPSHOT-jar-with-dependencies.jar src/main/java/*.java
```

## Usage ##
```
java -cp build:./lib/bioLQM-0.5-SNAPSHOT-jar-with-dependencies.jar:./lib/GINsim-2.9.6-SNAPSHOT-jar-with-dependencies.jar main.java.Main <model-in-ginml> <perturbations-file> <drugPanel-file> -output <output-filename>
```

example: 
```
java -cp build:./lib/bioLQM-0.5-SNAPSHOT-jar-with-dependencies.jar:./lib/GINsim-2.9.6-SNAPSHOT-jar-with-dependencies.jar main.java.Main examples/regulatoryGraph.ginml examples/perturbations_sintef.tab examples/drugpanel_sintef.txt -output test.output
```
